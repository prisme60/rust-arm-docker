# rust-arm-docker

A docker that permits to compile in nightly version for :
- arm-unknown-linux-musleabihf
- arm-unknown-linux-gnueabihf
- x86_64-unknown-linux-gnu

This image is based on the image of Piers Finlayson on the stable Rust. Thanks for his good job.
[Rust compilation for Raspberry pi Zero and armv6](https://piers.rocks/docker/containers/raspberry/pi/rust/cross/compile/compilation/2018/12/16/rust-compilation-for-raspberry-pi.html)