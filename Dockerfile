FROM piersfinlayson/build:latest

LABEL maintainer="Christian FOUCHER <christian.foucher@gmail.com>"

RUN rustup update && \
    rustup install nightly && \
    rustup override set nightly && \
    rustup target add arm-unknown-linux-musleabihf && \
    rustup target add arm-unknown-linux-gnueabihf

